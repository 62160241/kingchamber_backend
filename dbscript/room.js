/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Room = require('../models/Room')
const Equipment = require('../models/Equipment')
const Building = require('../models/Building')
const OrderConsideration = require('../models/Order_Consideration')
mongoose.connect('mongodb://localhost:27017/data')

async function clearDB() {
  await Room.deleteMany({})
}

async function main() {
  await clearDB()
  const Projector = await Equipment.findById({ _id: '6255a9cc2126c960a403b074' }).exec()
  const Computer = await Equipment.findById({ _id: '6255a9cc2126c960a403b075' }).exec()
  const Informatics = await Building.findById({ _id: '6255ffba5e2dcf0193d2e642' }).exec()
  const obj = await OrderConsideration.findById({ _id: '6255d87aed1c50928616ad8d' }).exec()
  const IF3M210 = new Room({ Room_ID: '3M210', Type: 'Slide Room', Size: 120, Facility: [Projector, Computer], Info_room: 'Slide room multi purpose for lecture class.', Building: Informatics, Order_Consideration: obj })
  IF3M210.save()
}

main().then(function () {
  console.log('Finish')
})
