/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/data')

async function cleanUsers() {
  await User.deleteMany({})
}

async function main() {
  await cleanUsers()

  const admin = new User({ firstname: 'admin', lastname: 'admin', roles: [ROLE.ADMIN], faculty: 'IF', username: 'admin', password: '1234' })
  admin.save()
  const adminFac = new User({ firstname: 'adminFac', lastname: 'adminFac', roles: [ROLE.ADMIN_FACULTY], faculty: 'IF', username: 'admin_fac', password: '1234' })
  adminFac.save()
  const user = new User({ firstname: 'user', lastname: 'user', roles: [ROLE.USER], faculty: 'IF', username: 'user', password: '1234' })
  user.save()
  const approver = new User({ firstname: 'approver', lastname: 'approver', roles: [ROLE.APPROVER], faculty: 'IF', username: 'approver', password: '1234' })
  approver.save()
  const nitro = new User({ firstname: 'Nitro', lastname: 'Ez', roles: [ROLE.ADMIN], faculty: 'IF', username: 'NitroEz', password: '1234' })
  nitro.save()
}

main().then(function () {
  console.log('Finish')
})
