/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Faculty = require('../models/Faculty')
mongoose.connect('mongodb://localhost:27017/data')

async function cleanFaculty() {
  await Faculty.deleteMany({})
}

async function main() {
  await cleanFaculty()

  const informatics = new Faculty({ name: 'Informatics' })
  informatics.save()

  const medicine = new Faculty({ name: 'Medicine' })
  medicine.save()

  const pharmacy = new Faculty({ name: 'Pharmacy' })
  pharmacy.save()

  const thaiTraditionalMedicine = new Faculty({ name: 'Thai Traditional Medicine' })
  thaiTraditionalMedicine.save()

  const nursing = new Faculty({ name: 'Nursing' })
  nursing.save()

  const humanities = new Faculty({ name: 'Humanities and Social Sciences' })
  humanities.save()

  const geography = new Faculty({ name: 'Geography' })
  geography.save()

  const management = new Faculty({ name: 'Management and Tourism' })
  management.save()

  const political = new Faculty({ name: 'Political Science and Law' })
  political.save()

  const science = new Faculty({ name: 'Science' })
  science.save()

  const engineering = new Faculty({ name: 'Engineering' })
  engineering.save()

  const publicHealth = new Faculty({ name: 'Public Health' })
  publicHealth.save()

  const arts = new Faculty({ name: 'Arts' })
  arts.save()

  const music = new Faculty({ name: 'Music and Performance' })
  music.save()

  const education = new Faculty({ name: 'Education' })
  education.save()

  const logistics = new Faculty({ name: 'Logistics' })
  logistics.save()

  const sportsScience = new Faculty({ name: 'Sports Science' })
  sportsScience.save()

  const international = new Faculty({ name: 'International college' })
  international.save()

  const alliedHealthSciences = new Faculty({ name: 'Allied Health Sciences' })
  alliedHealthSciences.save()
}

main().then(function () {
  console.log('Finish')
})
