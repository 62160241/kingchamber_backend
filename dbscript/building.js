/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Building = require('../models/Building')
mongoose.connect('mongodb://localhost:27017/data')
const Faculty = require('../models/Faculty')

async function cleanBuilding() {
  await Building.deleteMany({})
}

async function main() {
  await cleanBuilding()
  const inf = await Faculty.findById({ _id: '62360795ea7c1afb6d8d881d' }).exec()
  // console.log(inf)
  const IF = new Building({ Building_ID: 'IF', Building_Name: 'Informatics', faculty: inf, Room: [] })
  IF.save()
}

main().then(function () {
  console.log('Finish')
})
