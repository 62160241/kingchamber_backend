/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Reserve = require('../models/Reserve')
const Room = require('../models/Room')
const User = require('../models/User')
const Approver = require('../models/Approver')
mongoose.connect('mongodb://localhost:27017/data')

async function cleanReserve() {
  await Reserve.deleteMany({})
}
async function main() {
  // await cleanReserve()
  const IF3M210 = await Room.findById({ _id: '625861b0cd69093a712b6763' }).exec()
  const user = await User.findById({ _id: '6236084c9809b4d3f7b71225' }).exec()
  const app1 = await Approver.findById({ _id: '625b09cedefc69e325cfa454' }).exec()
  const app2 = await Approver.findById({ _id: '625b09cedefc69e325cfa455' }).exec()
  const app3 = await Approver.findById({ _id: '625b10834a233d7abd5a8c2c' }).exec()
  const app4 = await Approver.findById({ _id: '625b10834a233d7abd5a8c2d' }).exec()
  await Reserve.insertMany([
    {
      title: 'Title 8',
      content: 'Content 8',
      class: 'a',
      createDate: new Date('2022-05-02 09:00'),
      startDate: new Date('2022-05-09 08:00'),
      endDate: new Date('2022-05-09 16:00'),
      Room: IF3M210,
      User: user,
      requirement: 'for education purpose.',
      approver: [app1, app2],
      status: 'wait'
    },
    {
      title: 'Title 9',
      content: 'Content 9',
      class: 'b',
      createDate: new Date('2022-05-02 09:00'),
      startDate: new Date('2022-05-10 08:00'),
      endDate: new Date('2022-05-10 16:00'),
      Room: IF3M210,
      User: user,
      requirement: 'for education purpose.',
      approver: [app1, app3],
      status: 'wait'
    },
    {
      title: 'Title 10',
      content: 'Content 10',
      class: 'b',
      createDate: new Date('2022-05-02 09:00'),
      startDate: new Date('2022-05-11 08:00'),
      endDate: new Date('2022-05-11 16:00'),
      Room: IF3M210,
      User: user,
      requirement: 'for education purpose.',
      approver: [app3, app4],
      status: 'wait'
    },
    {
      title: 'Title 11',
      content: 'Content 11',
      class: 'b',
      createDate: new Date('2022-05-02 09:00'),
      startDate: new Date('2022-05-11 08:00'),
      endDate: new Date('2022-05-11 16:00'),
      Room: IF3M210,
      User: user,
      requirement: 'for education purpose.',
      approver: [app2, app4],
      status: 'wait'
    }
  ])
  Reserve.find({})
}

main().then(function () {
  console.log('Finish')
})
