/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Equipment = require('../models/Equipment')
mongoose.connect('mongodb://localhost:27017/data')

async function cleanEquipment() {
  await Equipment.deleteMany({})
}

async function main() {
  await cleanEquipment()

  const projector = new Equipment({ name: 'Projector' })
  projector.save()
  const computer = new Equipment({ name: 'Computer' })
  computer.save()
  const chair = new Equipment({ name: 'Chair' })
  chair.save()
}

main().then(function () {
  console.log('Finish')
})
