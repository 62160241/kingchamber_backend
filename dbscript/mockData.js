/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant')
const Faculty = require('../models/Faculty')
const Order = require('../models/Order_Consideration')
const Equipment = require('../models/Equipment')
const Building = require('../models/Building')
const Room = require('../models/Room')
const Reserve = require('../models/Reserve')
mongoose.connect('mongodb://localhost:27017/data')

async function cleanDB() {
  await User.deleteMany({})
  await Faculty.deleteMany({})
  await Order.deleteMany({})
  await Equipment.deleteMany({})
  await Building.deleteMany({})
  await Room.deleteMany({})
  await Reserve.deleteMany({})
}

async function main() {
  await cleanDB()

  const informatics = new Faculty({ name: 'Informatics' })
  informatics.save()

  const medicine = new Faculty({ name: 'Medicine' })
  medicine.save()

  const pharmacy = new Faculty({ name: 'Pharmacy' })
  pharmacy.save()

  const thaiTraditionalMedicine = new Faculty({ name: 'Thai Traditional Medicine' })
  thaiTraditionalMedicine.save()

  const nursing = new Faculty({ name: 'Nursing' })
  nursing.save()

  const humanities = new Faculty({ name: 'Humanities and Social Sciences' })
  humanities.save()

  const geography = new Faculty({ name: 'Geography' })
  geography.save()

  const management = new Faculty({ name: 'Management and Tourism' })
  management.save()

  const political = new Faculty({ name: 'Political Science and Law' })
  political.save()

  const science = new Faculty({ name: 'Science' })
  science.save()

  const engineering = new Faculty({ name: 'Engineering' })
  engineering.save()

  const publicHealth = new Faculty({ name: 'Public Health' })
  publicHealth.save()

  const arts = new Faculty({ name: 'Arts' })
  arts.save()

  const music = new Faculty({ name: 'Music and Performance' })
  music.save()

  const education = new Faculty({ name: 'Education' })
  education.save()

  const logistics = new Faculty({ name: 'Logistics' })
  logistics.save()

  const sportsScience = new Faculty({ name: 'Sports Science' })
  sportsScience.save()

  const international = new Faculty({ name: 'International college' })
  international.save()

  const alliedHealthSciences = new Faculty({ name: 'Allied Health Sciences' })
  alliedHealthSciences.save()

  // admin
  const admin = new User({ firstname: 'admin', lastname: 'admin', roles: [ROLE.ADMIN], faculty: informatics, username: 'admin', password: '1234' })
  // admin_fac
  const adminIF = new User({ firstname: 'adminIF', lastname: 'adminIF', roles: [ROLE.ADMIN_FACULTY], faculty: informatics, username: 'admin_IF', password: '1234' })
  const adminSCI = new User({ firstname: 'adminSCI', lastname: 'adminSCI', roles: [ROLE.ADMIN_FACULTY], faculty: science, username: 'admin_SCI', password: '1234' })
  const user = new User({ firstname: 'user', lastname: 'user', roles: [ROLE.USER], faculty: informatics, username: 'user', password: '1234' })
  const approverIF = new User({ firstname: 'approverIF', lastname: 'approver', roles: [ROLE.APPROVER], faculty: informatics, username: 'approverIF', password: '1234' })
  const nitro = new User({ firstname: 'Nitro', lastname: 'Ez', roles: [ROLE.ADMIN], faculty: informatics, username: 'NitroEz', password: '1234' })
  // user 10
  const sung = new User({ firstname: 'sung', lastname: 'sung', roles: [ROLE.USER], faculty: informatics, username: 'sung', password: '1234' })
  const captain = new User({ firstname: 'captain', lastname: 'captain', roles: [ROLE.USER], faculty: informatics, username: 'captain', password: '1234' })
  const leyla = new User({ firstname: 'leyla', lastname: 'leyla', roles: [ROLE.USER], faculty: informatics, username: 'leyla', password: '1234' })
  const pond = new User({ firstname: 'pond', lastname: 'pond', roles: [ROLE.USER], faculty: informatics, username: 'pond', password: '1234' })
  const kongky = new User({ firstname: 'kongky', lastname: 'kongky', roles: [ROLE.USER], faculty: informatics, username: 'kongky', password: '1234' })
  const tom = new User({ firstname: 'tom', lastname: 'tom', roles: [ROLE.USER], faculty: science, username: 'tom', password: '1234' })
  const omsin = new User({ firstname: 'omsin', lastname: 'omsin', roles: [ROLE.USER], faculty: logistics, username: 'omsin', password: '1234' })
  const orn = new User({ firstname: 'orn', lastname: 'orn', roles: [ROLE.USER], faculty: arts, username: 'orn', password: '1234' })
  const tiktok = new User({ firstname: 'tiktok', lastname: 'tiktok', roles: [ROLE.USER], faculty: music, username: 'tiktok', password: '1234' })
  const mery = new User({ firstname: 'mery', lastname: 'mery', roles: [ROLE.USER], faculty: nursing, username: 'mery', password: '1234' })

  // apv 5
  const approverSCI = new User({ firstname: 'approver1', lastname: 'approver', roles: [ROLE.APPROVER], faculty: informatics, username: 'approver1', password: '1234' })
  const approverLGT = new User({ firstname: 'approver2', lastname: 'approver', roles: [ROLE.APPROVER], faculty: informatics, username: 'approver2', password: '1234' })
  const approverART = new User({ firstname: 'approver3', lastname: 'approver', roles: [ROLE.APPROVER], faculty: informatics, username: 'approver3', password: '1234' })
  const approverMS = new User({ firstname: 'approver4', lastname: 'approver', roles: [ROLE.APPROVER], faculty: music, username: 'approver4', password: '1234' })
  const approverN = new User({ firstname: 'approver5', lastname: 'approver', roles: [ROLE.APPROVER], faculty: nursing, username: 'approver5', password: '1234' })
  // adminFaculty 1
  const adminLogistic = new User({ firstname: 'adminLGT', lastname: 'adminLGT', roles: [ROLE.ADMIN_FACULTY], faculty: logistics, username: 'admin_LGT', password: '1234' })

  // approval
  const app1 = new Order({ Order_ID: '0001', Order_Name: 'ลำดับอนุมัติเลขที่ 1', User: [approverSCI, approverLGT] })
  const app2 = new Order({ Order_ID: '0002', Order_Name: 'ลำดับอนุมัติเลขที่ 2', User: [approverLGT, approverART] })
  const app3 = new Order({ Order_ID: '0003', Order_Name: 'ลำดับอนุมัติเลขที่ 3', User: [approverSCI, approverLGT, approverART] })
  // eqipment
  const projector = new Equipment({ name: 'Projector' })
  const computer = new Equipment({ name: 'Computer' })
  const chair = new Equipment({ name: 'Chair' })
  // Building
  const buildingInformatics = new Building({ Building_ID: 'IF', Building_Name: 'ตึกคณะวิทยาการสารสนเทศ', faculty: informatics, Room: [] })
  const buildingScience = new Building({ Building_ID: 'SCI', Building_Name: 'ตึกคณะวิทยาศาสตร์', faculty: science, Room: [] })
  const buildingEngineering = new Building({ Building_ID: 'EG', Building_Name: 'ตึกคณะวิศวกรรมศาสตร์', faculty: engineering, Room: [] })
  // Room
  // const Room3C210 = new Room({ Room_ID: '3C210', Type: 'Slide Room', Size: 120, Facility: [projector, computer], Info:'data',  })

  await admin.save()
  await adminIF.save()
  await adminSCI.save()
  await user.save()
  await approverIF.save()
  await nitro.save()
  await adminLogistic.save()
  await approverSCI.save()
  await approverLGT.save()
  await approverART.save()
  await approverMS.save()
  await approverN.save()
  await sung.save()
  await captain.save()
  await leyla.save()
  await pond.save()
  await kongky.save()
  await tom.save()
  await omsin.save()
  await orn.save()
  await tiktok.save()
  await mery.save()
  await app1.save()
  await app2.save()
  await app3.save()
  await projector.save()
  await computer.save()
  await chair.save()
  await buildingInformatics.save()
  await buildingScience.save()
  await buildingEngineering.save()
}

main().then(function () {
  console.log('Finish')
})
