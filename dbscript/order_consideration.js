/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const Orderconsider = require('../models/Order_Consideration')
mongoose.connect('mongodb://localhost:27017/data')
const User = require('../models/User')

async function cleanOrder() {
  await Orderconsider.deleteMany({})
}

async function main() {
  await cleanOrder()
  const User1 = await User.findById({ _id: '6236084c9809b4d3f7b71223' }).exec()
  const User2 = await User.findById({ _id: '6236084c9809b4d3f7b71222' }).exec()
  const User3 = await User.findById({ _id: '6236084c9809b4d3f7b71224' }).exec()

  const order1 = new Orderconsider({ Order_ID: '0001', Order_Name: 'ลำดับการอนุมัติที่ 1', User: [User1, User2] })
  order1.save()
  const order2 = new Orderconsider({ Order_ID: '0002', Order_Name: 'ลำดับการอนุมัติที่ 2', User: [User2, User3] })
  order2.save()
  const order3 = new Orderconsider({ Order_ID: '0003', Order_Name: 'ลำดับการอนุมัติที่ 3', User: [User1, User2, User3] })
  order3.save()
}

main().then(function () {
  console.log('Finish')
})
