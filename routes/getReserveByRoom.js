const express = require('express')
// const { $where } = require('../models/Reserve')
const router = express.Router()
const Reserve = require('../models/Reserve')
// const Room = require('../models/Room')
// const mongoose = require('mongoose')

const getReserve = async function (req, res, next) {
  const id = req.params.id
  try {
    const reserve = await Reserve.find({ Room: { _id: id }, status: 'Approved' }).populate('User').populate('Room')
    console.log(reserve)
    if (reserve === null) {
      res.status(404).json({
        message: 'Reserve not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const getReservesRoomId = async function (req, res, next) {
  const id = req.params.id
  const startDate = req.query.startDate
  const endDate = req.query.endDate
  try {
    const reserve = await Reserve.find({ Room: { _id: id }, status: ['Approved', 'Waiting for Approval'], $or: [{ startDate: { $gte: startDate, $lt: endDate } }, { endDate: { $gte: startDate, $lt: endDate } }] }).populate('User').populate('Room')
    console.log(reserve)
    if (reserve === null) {
      res.status(404).json({
        message: 'Reserve not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}
router.get('/:id', getReserve)
router.get('/rooms/:id', getReservesRoomId)

module.exports = router
