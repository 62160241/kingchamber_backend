const express = require('express')
const router = express.Router()
const User = require('../models/User')

const getApprover = async function (req, res, next) {
  try {
    const user = await User.find({ roles: 'APPROVER' })
    console.log(user)
    if (user === null) {
      res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/', getApprover)

module.exports = router
