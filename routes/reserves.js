/* eslint-disable indent */
const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')
const Room = require('../models/Room')
const Order = require('../models/Order_Consideration')
const mongoose = require('mongoose')
// const User = require('../models/User')

const getReserves = async function (req, res, next) {
  try {
    console.log(req.query)
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const events = await Reserve.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } }, { endDate: { $gte: startDate, $lt: endDate } }]
    }).populate('User').populate('Room')
    res.status(200).json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
// find by user(approver) id
const getReserve = async function (req, res, next) {
  const id = req.params.id
  try {
    const reserve = await Reserve.find({ 'approver.User': mongoose.Types.ObjectId(id) }).populate('User').populate('Room')
    // console.log(reserve)
    if (reserve === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addReserves = async function (req, res, next) {
  const room = await Room.findById(req.body.Room)
  const startDate = req.body.start
  const endDate = req.body.end
  // console.log('----room----\n')
  // console.log(room)
  // console.log('\n----date----\n')
  // console.log(startDate)
  // console.log(endDate)

  const order = await Order.findById(room.Order_Consideration)
  const preReserve = await Reserve.find({
    $or: [{ startDate: { $gte: startDate, $lt: endDate } },
    { endDate: { $gte: startDate, $lt: endDate }, Room: room }]
  }).exec()
  const preRoom = await Reserve.find({ Room: room }).exec()
  // console.log('\n----preReserve----\n')
  // console.log(preReserve)
  // console.log('\n----preRoom----\n')
  // console.log(preRoom)
  if (preReserve.length !== 0) {
    if (preRoom.length !== 0) {
      // console.log('Yeah! you can do it!')
      console.log('Reject room has been reserve by other.')
      return res.status(406).send({
        message: 'Room is reserve by other.'
      })
    }
  }

  const approver = []
  order.User.forEach(element => {
    approver.push({ User: element._id, status: 'Waiting for Approval' })
  })
  const newreserve = new Reserve({
    title: req.body.title,
    content: req.body.content,
    startDate: req.body.start,
    endDate: req.body.end,
    class: req.body.class,
    createDate: req.body.createDate,
    Room: req.body.Room,
    User: req.body.User,
    requirement: req.body.requirement,
    approver: approver,
    status: req.body.status
  })
  try {
    await newreserve.save()
    res.status(201).json(newreserve)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateReserves = async function (req, res, next) {
  // console.log(req.params)
  const user = req.body.User
  const status = req.body.status
  const ReserveId = req.params.id
  // console.log(ReserveId)
  // console.log(user)
  // console.log(status)

  const reserve = await Reserve.findById(ReserveId)
  const approver = reserve.approver
  const approv = JSON.parse(JSON.stringify(approver))
  // console.log(approv)
  // console.log('--Process--')
  approv.forEach(element => {
    if (element.User === user) {
      element.status = status
    }
    element.User = mongoose.Types.ObjectId(element.User)
  })

  console.log(approv)

  try {
    // eslint-disable-next-line no-unused-vars
    let check = 0
    approv.forEach(element => {
      if (element.status === 'Disapproved') {
        reserve.status = 'Disapproved'
        reserve.class = 'c'
      }
      if (element.status === 'Approved') {
        check++
      }

      if (approv.length === check) {
        reserve.status = 'Approved'
        reserve.class = 'b'
      }
    })
    reserve.approver = approv
    // console.log(reserve.approver)
    await reserve.save()
    return res.status(200).json(reserve)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}
/*
const ReserveId = req.query.id
  const user = req.query.UserID
  const status = req.query.status
  const reserve = await Reserve.findById(ReserveId)
  const approver = reserve.approver
  */

/*
const deleteReserves = async function (req, res, next) {
  const reserveId = req.params.id
  try {
    await Reserve.findByIdAndDelete(reserveId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
*/
router.get('/', getReserves) // GET All User

router.get('/:id', getReserve) // GET One User
router.post('/', addReserves) // Add New User

router.put('/:id', updateReserves) // Update User
/*
router.delete('/:id', deleteReserves) // Delete User
*/
module.exports = router
