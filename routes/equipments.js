const express = require('express')
const router = express.Router()
const Equipment = require('../models/Equipment')

const getEquipments = async function (req, res, next) {
  try {
    const equipments = await Equipment.find({}).exec()
    res.status(200).json(equipments)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getEquipment = async function (req, res, next) {
  const id = req.params.id
  try {
    const equipment = await Equipment.findById(id).exec()
    if (equipment === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(equipment)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addEquipments = async function (req, res, next) {
  const newEquipment = new Equipment({
    name: req.body.name
  })
  try {
    await newEquipment.save()
    res.status(201).json(newEquipment)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateEquipments = async function (req, res, next) {
  const equipmentId = req.params.id
  try {
    const equipment = await Equipment.findById(equipmentId)
    equipment.name = req.body.name
    await equipment.save()
    return res.status(200).json(equipment)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteEquipments = async function (req, res, next) {
  const equipmentId = req.params.id
  try {
    await Equipment.findByIdAndDelete(equipmentId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getEquipments) // GET USER
router.get('/:id', getEquipment) // GET One User
router.post('/', addEquipments) // Add New User
router.put('/:id', updateEquipments) // Update User
router.delete('/:id', deleteEquipments) // Delete User

module.exports = router
