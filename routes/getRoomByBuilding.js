const express = require('express')
const router = express.Router()
const Building = require('../models/Building')
// const mongoose = require('mongoose')

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const building = await Building.findById(id).populate('Room')
    console.log(building)
    const Rooms = building.Room
    console.log(Rooms)
    if (Rooms === null) {
      res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(Rooms)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getRoom)

module.exports = router
