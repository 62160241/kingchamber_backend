const express = require('express')
const router = express.Router()
const Faculty = require('../models/Faculty')

const getFaculties = async function (req, res, next) {
  try {
    const faculty = await Faculty.find({}).exec()
    res.status(200).json(faculty)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getFaculty = async function (req, res, next) {
  const id = req.params.id
  try {
    const faculty = await Faculty.findById(id).exec()
    if (faculty === null) {
      res.status(404).json({
        message: 'Faculty not found!!'
      })
    }
    res.json(faculty)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addFaculties = async function (req, res, next) {
  const newFaculty = new Faculty({
    name: req.body.name
  })
  try {
    await newFaculty.save()
    res.status(201).json(newFaculty)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateFaculties = async function (req, res, next) {
  const equipmentId = req.params.id
  try {
    const equipment = await Faculty.findById(equipmentId)
    equipment.name = req.body.name
    await equipment.save()
    return res.status(200).json(equipment)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteFaculty = async function (req, res, next) {
  const equipmentId = req.params.id
  try {
    await Faculty.findByIdAndDelete(equipmentId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getFaculties) // GET USER
router.get('/:id', getFaculty)
router.post('/', addFaculties) // Add New User
router.put('/:id', updateFaculties) // Update User
router.delete('/:id', deleteFaculty) // Delete User

module.exports = router
