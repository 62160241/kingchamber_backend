const express = require('express')
const router = express.Router()
const Room = require('../models/Room')

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const room = await Room.findById(id).populate('Building').populate('Order_Consideration')
    if (room === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getRoom)
module.exports = router
