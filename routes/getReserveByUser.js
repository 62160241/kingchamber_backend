const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')

const getReserve = async function (req, res, next) {
  const id = req.params.id
  try {
    const reserve = await Reserve.find({ User: { _id: id } }).populate('User').populate('Room')
    console.log(reserve)
    if (reserve === null) {
      res.status(404).json({
        message: 'Reserve not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getReserve)

module.exports = router
