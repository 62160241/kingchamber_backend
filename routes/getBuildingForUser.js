const express = require('express')
const router = express.Router()
const Building = require('../models/Building')

const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).populate('faculty')
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

router.get('/', getBuildings) // GET USER

module.exports = router
