const express = require('express')
const router = express.Router()
const User = require('../models/User')
const Faculty = require('../models/Faculty')

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).populate('faculty')
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).populate('faculty')
    if (user === null) {
      res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

// const addUsers = async function (req, res, next) {
//   const newUser = new User({
//     firstname: req.body.firstname,
//     lastname: req.body.lastname,
//     roles: req.body.roles,
//     faculty: req.body.faculty,
//     username: req.body.username,
//     password: req.body.password
//   })
//   try {
//     await newUser.save()
//     res.status(201).json(newUser)
//   } catch (err) {
//     return res.status(500).send({
//       message: err.message
//     })
//   }
// }

// Test add
const addUsers = async function (req, res, next) {
  const facultyFind = await Faculty.findById(req.body.faculty)
  const newUser = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    roles: req.body.roles,
    faculty: facultyFind,
    username: req.body.username,
    password: req.body.password
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateUsers = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.firstname = req.body.firstname
    user.lastname = req.body.lastname
    user.roles = req.body.roles
    user.faculty = req.body.faculty
    user.username = req.body.username
    user.password = req.body.password
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteUsers = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getUsers) // GET USER
router.get('/:id', getUser) // GET One User
router.post('/', addUsers) // Add New User
router.put('/:id', updateUsers) // Update User
router.delete('/:id', deleteUsers) // Delete User

module.exports = router
