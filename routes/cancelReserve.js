const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')

const updateReserves = async function (req, res, next) {
  const ReserveId = req.params.id

  const reserve = await Reserve.findById(ReserveId)

  try {
    reserve.status = 'Cancel'
    await reserve.save()
    return res.status(200).json(reserve)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

router.put('/:id', updateReserves) // Update User

module.exports = router
