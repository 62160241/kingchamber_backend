const express = require('express')
const router = express.Router()
const Room = require('../models/Room')
const Building = require('../models/Building')

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).populate('Building').populate('Order_Consideration')
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const room = await Room.findById(id).populate('Building').populate('Order_Consideration')
    if (room === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addRooms = async function (req, res, next) {
  const newRoom = new Room({
    Room_ID: req.body.Room_ID,
    Type: req.body.Type,
    Size: req.body.Size,
    Facility: req.body.Facility,
    Info_room: req.body.Info_room,
    Building: req.body.Building,
    Order_Consideration: req.body.Order_Consideration
  })
  try {
    const buildingFind = await Building.findById(newRoom.Building)
    buildingFind.Room.push(newRoom)
    await newRoom.save()
    await buildingFind.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRooms = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    const oldBuildingFind = await Building.findById(room.Building)
    // console.log(oldBuildingFind)

    room.Room_ID = req.body.Room_ID
    room.Type = req.body.Type
    room.Size = req.body.Size
    room.Facility = req.body.Facility
    room.Info_room = req.body.Info_room
    room.Building = req.body.Building
    room.OrderConsideration = req.body.OrderConsideration

    if (oldBuildingFind !== req.body.Building) {
      oldBuildingFind.Room.pull(roomId)
      const newBuildingFind = await Building.findById(req.body.Building)
      newBuildingFind.Room.push(room)
      newBuildingFind.save()
    }
    await oldBuildingFind.save()
    await room.save()
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteRooms = async function (req, res, next) {
  const roomId = req.params.id
  const roomFind = await Room.findById(roomId)
  const oldBuildingFind = await Building.findById(roomFind.Building)
  try {
    await Room.findByIdAndDelete(roomId)
    oldBuildingFind.Room.pull(roomId)
    await oldBuildingFind.save()
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getRooms) // GET USER
router.get('/:id', getRoom) // GET One User
router.post('/', addRooms) // Add New User
router.put('/:id', updateRooms) // Update User
router.delete('/:id', deleteRooms) // Delete User

module.exports = router
