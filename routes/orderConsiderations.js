/* eslint-disable space-before-function-paren */
const express = require('express')
const router = express.Router()
const Order = require('../models/Order_Consideration')

const getOrders = async function (req, res, next) {
  try {
    const orders = await Order.find({})
    res.status(200).json(orders)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getOrder = async function (req, res, next) {
  const id = req.params.id
  try {
    const order = await Order.findById(id)
    if (order === null) {
      res.status(404).json({
        message: 'Order not found!!'
      })
    }
    res.json(order)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addOrders = async function (req, res, next) {
  const newOrder = new Order({
    Order_ID: req.body.Order_ID,
    Order_Name: req.body.Order_Name,
    User: req.body.User
  })
  try {
    await newOrder.save()
    res.status(201).json(newOrder)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateOrders = async function (req, res, next) {
  const orderId = req.params.id
  try {
    const order = await Order.findById(orderId)
    order.Order_ID = req.body.Order_ID
    order.Order_Name = req.body.Order_Name
    order.User = req.body.User
    await order.save()
    return res.status(200).json(order)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteOrders = async function (req, res, next) {
  const orderId = req.params.id
  try {
    await Order.findByIdAndDelete(orderId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getOrders) // GET USER
router.get('/:id', getOrder) // GET One User
router.post('/', addOrders) // Add New User
router.put('/:id', updateOrders) // Update User
router.delete('/:id', deleteOrders) // Delete User

module.exports = router
