const express = require('express')
const router = express.Router()
const Building = require('../models/Building')
const Room = require('../models/Room')
const Faculty = require('../models/Faculty')

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const faculty = await Faculty.findById(id)
    const building = await Building.find({ faculty: faculty }).populate('Room')
    const rooms = await Room.find({ Building: building }).populate('Building').populate('Order_Consideration')
    /*
    console.log('------faculty--------\n')
    console.log(faculty)
    console.log('\n------building--------\n')
    console.log(building)
    console.log('\n------room--------\n')
    console.log(rooms)
    */
    const Rooms = rooms
    if (Rooms === null) {
      res.status(404).json({
        message: 'rooms not found!!'
      })
    }
    res.json(Rooms)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getRoom)

module.exports = router
