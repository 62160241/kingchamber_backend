const ROLE = {
  ADMIN: 'ADMIN',
  ADMIN_FACULTY: 'ADMIN_FACULTY',
  USER: 'USER',
  APPROVER: 'APPROVER'
}

module.exports = { ROLE }
