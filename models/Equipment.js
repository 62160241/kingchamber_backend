const mongoose = require('mongoose')
const { Schema } = mongoose

const equipmentSchema = Schema({
  name: String
})

module.exports = mongoose.model('Equipment', equipmentSchema)
