const mongoose = require('mongoose')
const { Schema } = mongoose

const reserveSchema = Schema({
  title: String,
  content: String,
  class: String,
  createDate: Date,
  startDate: Date,
  endDate: Date,
  Room: { type: Schema.Types.ObjectId, ref: 'Room' },
  User: { type: Schema.Types.ObjectId, ref: 'User' },
  requirement: String,
  approver: [],
  status: String
}, {
  timeStamps: true
})

module.exports = mongoose.model('Reserve', reserveSchema)
