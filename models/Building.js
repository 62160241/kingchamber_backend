const mongoose = require('mongoose')
const { Schema } = mongoose

const buildingSchema = Schema({
  Building_ID: String,
  Building_Name: String,
  faculty: { type: Schema.Types.ObjectId, ref: 'Faculty' },
  Room: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }]
})

module.exports = mongoose.model('Building', buildingSchema)
