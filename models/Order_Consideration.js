const mongoose = require('mongoose')
const { Schema } = mongoose

const oderConsiderSchema = Schema({
  Order_ID: String,
  Order_Name: String,
  User: [{ type: Schema.Types.ObjectId, ref: 'User' }]
})

module.exports = mongoose.model('Order_Consideration', oderConsiderSchema)
