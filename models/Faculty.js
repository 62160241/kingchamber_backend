const mongoose = require('mongoose')
const { Schema } = mongoose

const facultySchema = Schema({
  name: String
})

module.exports = mongoose.model('Faculty', facultySchema)
