const mongoose = require('mongoose')
const { Schema } = mongoose

const roomSchema = Schema({
  Room_ID: String,
  Type: String,
  Size: Number,
  Facility: [{ type: Schema.Types.ObjectId, ref: 'Equipment' }],
  Info_room: String,
  Building: { type: Schema.Types.ObjectId, ref: 'Building' },
  Order_Consideration: { type: Schema.Types.ObjectId, ref: 'Order_Consideration' }
})

module.exports = mongoose.model('Room', roomSchema)
